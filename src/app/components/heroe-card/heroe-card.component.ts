import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-heroe-card',
  templateUrl: './heroe-card.component.html',
  styles: []
})
export class HeroeCardComponent implements OnInit {

  @Input() heroe: Heroe;
  @Input() nombre: string;

  @Output() heroeSeleccionado: EventEmitter<string>;

  constructor(private router: Router) {
    this.heroeSeleccionado = new EventEmitter();
  }

  ngOnInit() {
  }

  verHeroe(nombre: string) {
    //this.router.navigate(['/heroe', this.nombre]);
    this.heroeSeleccionado.emit( this.nombre );
  }

}
