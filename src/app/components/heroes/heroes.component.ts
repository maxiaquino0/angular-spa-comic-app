import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes: Heroe[] = [];
  termino: string;

  constructor(private _heroesService: HeroesService, private activatedRoute: ActivatedRoute, private router: Router) {

   }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.termino = params.termino;
      if (this.termino === '') {
        this.heroes = this._heroesService.getHeroes();
      } else {
        this.heroes = this._heroesService.buscarHeroe(this.termino);
      }
    });
  }

  verHeroe(nombre: string) {
    this.router.navigate(['/heroe', nombre]);
  }

}
